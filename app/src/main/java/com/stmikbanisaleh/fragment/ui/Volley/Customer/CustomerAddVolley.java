package com.stmikbanisaleh.fragment.ui.Volley.Customer;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stmikbanisaleh.fragment.Login;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Volley.RegisterVolley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CustomerAddVolley extends AppCompatActivity {
    private EditText nama_customer, tlp, email, alamat;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_add_volley);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.addCustomer) + "</font>"));
        nama_customer =  findViewById(R.id.txtName);
        tlp = findViewById(R.id.txtTlp);
        email = findViewById(R.id.txtEmail);
        alamat = findViewById(R.id.txtAlamat);
        btnSubmit = findViewById(R.id.save);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //    Toast.makeText(CustomerAddVolley.this, "Tambah", Toast.LENGTH_SHORT).show();
            addData();
            }
        });
    }


    public void addData(){

        String URL ="http://192.168.1.239/Api-Auth/public/api/auth/tambah-customer";
        SessionManager sessionManager = new SessionManager(this);
        String mToken = sessionManager.getStringData("access_token");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String Nama = this.nama_customer.getText().toString().trim();
        final String Telp = this.tlp.getText().toString();
        final String Email = this.email.getText().toString().trim();
        final String Alamat = this.alamat.getText().toString().trim();
        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(CustomerAddVolley.this, "Succes Add Data Customer", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(CustomerAddVolley.this, CustomerVolley.class);startActivity(intent); finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CustomerAddVolley.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Bearer" + mToken);
//                Log.d("token", "hasilnya Adalah   " + mToken);
                return headers;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> form = new HashMap<>();
                form.put("nama_customer", Nama);
                form.put("no_tlp", Telp);
                form.put("email", Email);
                form.put("alamat", Alamat);
                Log.d("tag", form.toString());
                return form;
            }

        };
        requestQueue.add(request);

    }

}
