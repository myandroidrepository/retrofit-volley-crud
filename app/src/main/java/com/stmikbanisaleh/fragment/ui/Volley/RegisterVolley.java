package com.stmikbanisaleh.fragment.ui.Volley;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stmikbanisaleh.fragment.Login;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.LoginVolley;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.CustomerAddVolley;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.CustomerVolley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterVolley extends AppCompatActivity {
    EditText textNama, textEmail, textPasword, txtperusahaan, txtalamat_perusahaan;
    Button btnRegister ; TextView btnlogin;
    RegisterVolley registerVolley ;

    private String URL_REGIST ="http://192.168.1.239/Api-Auth/public/api/auth/register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        textNama = (EditText) findViewById(R.id.name_text);
        textEmail = (EditText) findViewById(R.id.email_text);
        textPasword = (EditText) findViewById(R.id.password_text);
        btnRegister = (Button) findViewById(R.id.btn_register);
        btnlogin = (TextView) findViewById(R.id.login);
        txtperusahaan = (EditText) findViewById(R.id.perusahaan);
        txtalamat_perusahaan = (EditText) findViewById(R.id.alamat_perusahaan);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegisterUser();
            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterVolley.this, LoginVolley.class);
                startActivity(intent); finish();
            }
        });
    }



    private void RegisterUser(){
        String Nama = textNama.getText().toString();
        String Email = textEmail.getText().toString();
        String Password = textPasword.getText().toString();
        String NamaPerusahaan = txtperusahaan.getText().toString();
        String AlamatPerusahaan = txtalamat_perusahaan.getText().toString();
        String URL = " http://192.168.1.239/Api-Auth/public/api/auth/register";
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(RegisterVolley.this, "Succes Registrasi", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(RegisterVolley.this, LoginVolley.class);startActivity(intent); finish();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterVolley.this, "Gagal Registrasi", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> form = new HashMap<>();
                form.put("name", Nama);
                form.put("email", Email);
                form.put("password", Password);
                form.put("nama_perusahaan", NamaPerusahaan);
                form.put("alamat_perusahaan", AlamatPerusahaan);
                return form;
            }
        };
        requestQueue.add(request);
    }
}