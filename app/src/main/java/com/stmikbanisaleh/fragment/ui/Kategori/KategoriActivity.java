package com.stmikbanisaleh.fragment.ui.Kategori;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.stmikbanisaleh.fragment.Adapter.AdapterKategori;
import com.stmikbanisaleh.fragment.login.Kategori.DataKategori;
import com.stmikbanisaleh.fragment.login.Kategori.ResponKategori;
import com.stmikbanisaleh.fragment.Network.API;
import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KategoriActivity extends AppCompatActivity {

    private Button btn_add;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private API api;
    private AdapterKategori adapter;
    private List<DataKategori> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategori);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.kategori) + "</font>"));

        api = RetrofitClient.getInstance().getApi();
        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager =new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AdapterKategori(list);
        recyclerView.setAdapter(adapter);
        loadDataKategori();

        btn_add = findViewById(R.id.buttonAdd);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KategoriActivity.this, AddKategoriActivity.class);
                startActivity(intent); finish();
            }
        });
    }


    private void loadDataKategori(){
        Call<ResponKategori> call = api.getKategori();
        call.enqueue(new Callback<ResponKategori>() {
            @Override
            public void onResponse(Call<ResponKategori> call, Response<ResponKategori> response) {
                List<DataKategori> list = response.body().getData();

                Log.d("Retrofit Get", " Jumlah Kategori: " + (list.size()));
                adapter = new AdapterKategori(list);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<ResponKategori> call, Throwable t) {
                Toast.makeText(KategoriActivity.this, "Get Data Failed", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.menu_search);
        SearchView searchView =(SearchView) item.getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Cari Kategori");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    public boolean onOptionItemSelected(@NonNull MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(this, KategoriActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }



}