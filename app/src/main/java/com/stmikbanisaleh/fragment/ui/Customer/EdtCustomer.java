package com.stmikbanisaleh.fragment.ui.Customer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.CustomerActivity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EdtCustomer extends AppCompatActivity {
    //Declare variabel unruk menampung data dari data customer
    String  Nama_customer, No_telp, Email, Alamat;
 Integer Id ;
    private EditText edtId, edtNama_customer, edtTelp, edtEmail, edtAlamat;
    private Button btnSubmit;
    private Button btnDelete;
    SharedPreferences mypref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edt_customer);
//        Get Actionbar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.customer) + "</font>"));

        Id = getIntent().getIntExtra("id", 0);
        Nama_customer = getIntent().getStringExtra("nama_customer");
        No_telp = getIntent().getStringExtra("no_tlp");
        Email = getIntent().getStringExtra("email");
        Alamat = getIntent().getStringExtra("alamat");



        edtNama_customer =  findViewById(R.id.nama_customer);
        edtId = findViewById(R.id.id_cust);
        edtTelp = findViewById(R.id.telp);
        edtEmail = findViewById(R.id.email);
        edtAlamat= findViewById(R.id.alamat);
        btnSubmit = findViewById(R.id.submit);
        btnDelete = findViewById(R.id.delete);

        edtId.setText(Id+"");
        edtNama_customer.setText(Nama_customer);
        edtTelp.setText(No_telp);
        edtEmail.setText(Email);
        edtAlamat.setText(Alamat);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditAction();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionHapus();
            }
        });
    }
    public void EditAction(){
        Integer Id = Integer.parseInt(edtId.getText().toString());
        String Nama_customer = edtNama_customer.getText().toString().trim();
        String Email = edtEmail.getText().toString().trim();
        String Alamat = edtAlamat.getText().toString().trim();
        String Telp = edtTelp.getText().toString();

        //Validasi
        if(Nama_customer.isEmpty()){
            edtNama_customer.setError("Nama Harus Diisi!!!");
            edtNama_customer.requestFocus();
        }
        if(Email.isEmpty()){
            edtEmail.setError("Email Harus Diisi!!!");
            edtEmail.requestFocus();
        }
        if(Alamat.isEmpty()){
            edtAlamat.setError("Alamat Harus Diisi!!!");
            edtAlamat.requestFocus();

        }
        if(Telp.isEmpty()){
            edtTelp.setError("No Telp Harus Diisi!!!");
            edtTelp.requestFocus();
        }
        //Id = getIntent().getStringExtra("id");


        Call<ResponseBody> callEdit = RetrofitClient.getInstance().getApi().EditCustomer(Id, Nama_customer, No_telp, Email, Alamat);
        callEdit.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Toast.makeText(EdtCustomer.this, "Success Edit Data", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(EdtCustomer.this, CustomerActivity.class);
                startActivity(i);
                finish();
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EdtCustomer.this, "Hmmm Gagal", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void actionHapus(){
        Integer Id = Integer.parseInt(edtId.getText().toString());

        Call<ResponseBody> callDelete = RetrofitClient.getInstance().getApi().HapusCustomer(Id);
        callDelete.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Toast.makeText(EdtCustomer.this, "Success Delete", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(EdtCustomer.this, CustomerActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EdtCustomer.this, "Please Check Conection", Toast.LENGTH_LONG).show();

            }
        });
    }
}