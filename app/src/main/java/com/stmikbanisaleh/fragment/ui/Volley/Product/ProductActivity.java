package com.stmikbanisaleh.fragment.ui.Volley.Product;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.stmikbanisaleh.fragment.Adapter.AdaperProduct;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.login.Product.DataProduct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AdaperProduct adapter;
    private List<DataProduct> list = new ArrayList<>();
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product3);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.customer) + "</font>"));
        sessionManager = new SessionManager(this);

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager =new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AdaperProduct(list);
        recyclerView.setAdapter(adapter);
        LoadProductData();
    }
    private void LoadProductData(){
        String mToken = sessionManager.getStringData("access_token");
        String URL = "http://192.168.1.239/Api-Auth/public/api/auth/get-produk";
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                list = new ArrayList<>();
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray array = object.getJSONArray("data");
                    //JSONArray url = object.getJSONArray("url");
                    for(int i=0; i<array.length(); i++){
                        DataProduct model = new DataProduct();
                        JSONObject data = array.getJSONObject(i);
                        model.setId(data.getString("id"));
                        model.setNama_produk(data.getString("nama_produk"));
                        model.setId_kategori(data.getString("kategori_produk"));
                        model.setNama_kategori(data.getString("nama_kategori"));
                        model.setMerek(data.getString("nama_merek"));
                        model.setId_merek(data.getString("id_merek"));
                        model.setStok(data.getString("stok"));
                        model.setHarga(data.getString("harga"));
                        model.setImg(data.getString("img"));
                        list.add(model);
                    }
                    adapter = new AdaperProduct(list);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Bearer" + mToken);
               // Log.d("token", "hasilnya Adalah   " + mToken);
                return headers;
            }
        };
        requestQueue.add(request);

    }
}