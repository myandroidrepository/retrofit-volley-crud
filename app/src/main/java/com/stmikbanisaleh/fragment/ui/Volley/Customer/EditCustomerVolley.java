package com.stmikbanisaleh.fragment.ui.Volley.Customer;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditCustomerVolley extends AppCompatActivity {

    String NamaCust, TelpCust, AlamatCust, EmailCust, IdCust;
    EditText nama_customer, telp,alamat, email, id;
    Button EditCustomer, DeleteCustomer;
    SessionManager sessionManager ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_customer_volley);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.customer) + "</font>"));
        Bundle bundle = this.getIntent().getExtras();
        NamaCust = bundle.getString("nama_customer");

        IdCust = getIntent().getStringExtra("id");
       // NamaCust = getIntent().getStringExtra("no_tlp");
        TelpCust = getIntent().getStringExtra("no_tlp");
        EmailCust = getIntent().getStringExtra("email");
        AlamatCust = getIntent().getStringExtra("alamat");
        String coba = AlamatCust;
        sessionManager = new SessionManager(this);



        nama_customer = (EditText) findViewById(R.id.nama_customer);
        telp = (EditText) findViewById(R.id.telp);
        email = (EditText) findViewById(R.id.email);
        alamat = (EditText) findViewById(R.id.alamat);
        id = (EditText) findViewById(R.id.id_cust);

        id.setText(IdCust+"");
        nama_customer.setText(NamaCust);
        telp.setText(TelpCust);
        email.setText(EmailCust);
        alamat.setText(AlamatCust);

        EditCustomer = (Button) findViewById(R.id.EditCust);
        DeleteCustomer = (Button) findViewById(R.id.DeleteCust);
        EditCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditAction();
            }
        });
        DeleteCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               DeleteAction();
            }
        });
    }
    private void EditAction(){
        String NAMA = nama_customer.getText().toString();
        String ID = id.getText().toString().trim();
        String NO_TLP = telp.getText().toString().trim();
        String EMAIL = email.getText().toString().trim();
        String ALAMAT = alamat.getText().toString().trim();

        String URL = "http://192.168.1.239/Api-Auth/public/api/auth/update-customer/"+ID;
        String mToken = sessionManager.getStringData("access_token");
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest requestEdit = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String Message = object.getString("msg");
                    Toast.makeText(EditCustomerVolley.this, Message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(EditCustomerVolley.this, CustomerVolley.class);
                    startActivity(intent); finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EditCustomerVolley.this, "Failed Edit Data Customer", Toast.LENGTH_SHORT).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EditCustomerVolley.this, "Failed Edit Data Customer", Toast.LENGTH_SHORT).show();

            }
        }){
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + mToken);
                return headers;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> form = new HashMap<>();
                form.put("id", ID);
                form.put("nama_customer", NAMA);
                form.put("no_tlp", NO_TLP);
                form.put("email", EMAIL);
                form.put("alamat", ALAMAT);
                return form;
            }
        };
        requestQueue.add(requestEdit);


    }
    private void DeleteAction(){
        String ID = id.getText().toString().trim();
        String URL = "http://192.168.1.239/Api-Auth/public/api/auth/hapus-customer/"+ID;
        String mToken = sessionManager.getStringData("access_token");
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest requestDelete = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String Message = object.getString("msg");
                    Toast.makeText(EditCustomerVolley.this, Message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(EditCustomerVolley.this, CustomerVolley.class);
                    startActivity(intent); finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EditCustomerVolley.this, "Failed Delete Data Customer", Toast.LENGTH_SHORT).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EditCustomerVolley.this, "Failed Edit Data Customer", Toast.LENGTH_SHORT).show();

            }
        }){
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + mToken);
                return headers;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> form = new HashMap<>();
                form.put("id", ID);
                return form;
            }
        };
        requestQueue.add(requestDelete);
    }


}