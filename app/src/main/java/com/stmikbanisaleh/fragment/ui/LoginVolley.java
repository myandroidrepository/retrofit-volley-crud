package com.stmikbanisaleh.fragment.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stmikbanisaleh.fragment.MainActivity;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.login.ResponLogin;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Volley.RegisterVolley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginVolley extends AppCompatActivity {

    SessionManager sessionManager;

    private static String URL_LOGIN = "http://192.168.1.239/Api-Auth/public/api/auth/login";
    private SharedPreferences preferences;
    private EditText email_text, password_text;
    private ProgressBar pb_login;
    private Button btn_login ;
    TextView register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        sessionManager = new SessionManager(getApplicationContext());

        pb_login = (ProgressBar) findViewById(R.id.pb_login);
        email_text = (EditText) findViewById(R.id.email_text);
        password_text =(EditText) findViewById(R.id.password_text);
        btn_login = (Button) findViewById(R.id.btn_login);
        register = findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginVolley.this, RegisterVolley.class);
                startActivity(i); finish();
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txtEmail = email_text.getText().toString().trim();
                String txtPassword = password_text.getText().toString().trim();

                if(!txtEmail.isEmpty() || !txtPassword.isEmpty()){
                    LoginAction(txtEmail, txtPassword);
                }else {
                    email_text.setError("Plase Insert Email");
                    password_text.setError("Plase Insert Password");
                }
            }
        });
    }

    private void LoginAction(String email, String password) {
        pb_login.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String shortRespon = response.toString();
                //Log.d("respon", "coba:" +shortRespon );
                //sessionManager.setAccess_token(shortRespon);
                //sessionManager.createSession(email);
                try {

                    JSONObject jobj = new JSONObject(response);
                   String resultToken = jobj.getString("access_token");
                    //Log.e("TAG","Token "+resultToken.toString());
                    sessionManager.saveStringData("access_token", jobj.getString("access_token"));
                  //  Log.d("Session", "Session Token" + tokenSession);
                   sessionManager.createSession(resultToken);
                    pb_login.setVisibility(View.GONE);

                    Toast.makeText(LoginVolley.this, "Login Success", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginVolley.this, MainActivity.class); startActivity(intent); finish();
                } catch (JSONException e) {

                    e.printStackTrace();
                    pb_login.setVisibility(View.GONE);
                    btn_login.setVisibility(View.VISIBLE);
                    Toast.makeText(LoginVolley.this, "Login Failed", Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        btn_login.setVisibility(View.VISIBLE);
                        pb_login.setVisibility(View.GONE);
                        Toast.makeText(LoginVolley.this, "Plase Cek Your Connection", Toast.LENGTH_SHORT).show();

                    }
                })
        {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("Bearer", sessionManager.getStringData("access_token"));

                Log.d("tag", params.toString());
                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
