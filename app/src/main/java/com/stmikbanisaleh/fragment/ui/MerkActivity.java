package com.stmikbanisaleh.fragment.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stmikbanisaleh.fragment.Adapter.AdapterCust;
import com.stmikbanisaleh.fragment.Adapter.Volley.MerekAdapter;
import com.stmikbanisaleh.fragment.ModelVolley.Merek;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.CustomerAddVolley;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.CustomerVolley;
import com.stmikbanisaleh.fragment.ui.Volley.Merek.AddMerekActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MerkActivity extends AppCompatActivity {
        RecyclerView  recyclerView ;
        private RecyclerView.LayoutManager layoutManager;
        private List<Merek> list = new ArrayList<>() ;
       // private List<Merek> listmerek;
        private MerekAdapter adapter;
        SessionManager manager;
        private Button btn_add;
@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merk);

                ActionBar actionBar = getSupportActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.merek) + "</font>"));
                recyclerView = findViewById(R.id.recyclerView);
                LinearLayoutManager layoutManager =new LinearLayoutManager(this);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
                adapter = new MerekAdapter(list);
                recyclerView.setAdapter(adapter);
                btn_add = findViewById(R.id.buttonAdd);
                btn_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        Intent intent = new Intent(MerkActivity.this, AddMerekActivity.class);
                        startActivity(intent); finish();
                }

                });
                LoadDataMerek();

        }

        private void LoadDataMerek(){
                manager = new SessionManager(this);
                String mToken = manager.getStringData("access_token");

                String URL = "http://192.168.1.239/Api-Auth/public/api/auth/get-merek";
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                                list = new ArrayList<>();
                                try {
                                        JSONObject object = new JSONObject(response);
                                        String Message = object.getString("msg");
                                        JSONArray array = object.getJSONArray("data");
                                        for(int i=0; i<array.length(); i++){
                                                JSONObject data = array.getJSONObject(i);
                                                Merek model = new Merek();
                                                model.setNama_merek(data.getString("nama_merek"));
                                                model.setId(data.getString("id"));
                                                list.add(model);
                                        }
                                        adapter = new MerekAdapter(list);
                                        recyclerView.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();
                                } catch (JSONException e) {
                                        e.printStackTrace();
                                }
                        }
                }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                }){
                        @Override
                        public Map<String, String> getHeaders() {
                                Map<String, String> headers = new HashMap<>();
                                headers.put("Content-Type", "application/json; charset=UTF-8");
                                headers.put("Authorization", "Bearer" + mToken);
                                //Log.d("token", "hasilnya Adalah   " + mToken);
                                return headers;
                        }
                };
                requestQueue.add(request);
        }

}