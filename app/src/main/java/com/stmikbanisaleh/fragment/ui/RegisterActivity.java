package com.stmikbanisaleh.fragment.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.stmikbanisaleh.fragment.Login;
import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.R;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    EditText textNama, textEmail, textPasword;
    Button btnRegister, btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        textNama = (EditText) findViewById(R.id.name_text);
        textEmail = (EditText) findViewById(R.id.email_text);
        textPasword = (EditText) findViewById(R.id.password_text);
        btnRegister = (Button) findViewById(R.id.btn_register);
        btnlogin = (Button) findViewById(R.id.login);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionRegister();
            }
        });


    }

    private void actionRegister(){
        String Name = textNama.getText().toString().trim();
        String Email = textEmail.getText().toString().trim();
        String Password = textPasword.getText().toString().trim();

        if(Name.isEmpty()){
            textNama.setError("Plase Fill Your Name");
            textNama.requestFocus();
            return;
        }
        if(Email.isEmpty()){
            textEmail.setError("Plase Fill Your Email");
            textEmail.requestFocus();
            return;
        }
        if(Email.isEmpty() && Email.length() < 6){
            textPasword.setError("Plase Fill Your Password Minimum 6 Caracter");
            textPasword.requestFocus();
            return;
        }

        Call<ResponseBody> call= RetrofitClient.getInstance().getApi().RegisterUser(Name, Email, Password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() == 200){
                    ResponseBody responseBody = response.body();

                }
                // showDialog();
                Toast.makeText(RegisterActivity.this, "Success Regiter", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(RegisterActivity.this, Login.class);
                startActivity(i);
                finish();
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Add data Faile Plase Chek Your Connection!", Toast.LENGTH_SHORT).show();

            }
        });



    }
}