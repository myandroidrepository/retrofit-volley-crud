package com.stmikbanisaleh.fragment.ui.Volley.Merek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.MerkActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditHapusMerek extends AppCompatActivity {
    private EditText merek_name, merek_id;
    private Button ubah, hapus;
    private String id_merek, nama_merek;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_hapus_merek);
        sessionManager = new SessionManager(this);

        id_merek = getIntent().getStringExtra("id");
        nama_merek = getIntent().getStringExtra("nama_merek");

        merek_name = findViewById(R.id.nameMerek);
        merek_id = findViewById(R.id.idMerek);
        ubah = findViewById(R.id.editMerek);
        hapus = findViewById(R.id.btn_delete_merek);

        merek_name.setText(nama_merek);
        merek_id.setText(id_merek);

        ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ubahMerek();
            }
        });
        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hapusMerek();
            }
        });
    }

    private void ubahMerek(){
        String mToken = sessionManager.getStringData("access_token");
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String MerekNama = merek_name.getText().toString().trim();
        String MerekId = merek_id.getText().toString().trim();

        String URLEdit = "http://192.168.1.239/Api-Auth/public/api/auth/edit-merek/"+MerekId;
        StringRequest edit = new StringRequest(Request.Method.POST, URLEdit, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String Message = object.getString("msg");
                    Toast.makeText(EditHapusMerek.this, Message, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(EditHapusMerek.this, MerkActivity.class); startActivity(i); finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EditHapusMerek.this, "Edit Data Gagal", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EditHapusMerek.this, "Plase Chek Your Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + mToken);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> formEdit = new HashMap<>();
                formEdit.put("nama_merek", MerekNama);
                formEdit.put("id", MerekId);
                return formEdit;
            }
        };
        requestQueue.add(edit);

    }

    private void hapusMerek(){
        String mToken = sessionManager.getStringData("access_token");
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String MerekId = merek_id.getText().toString().trim();
        String URLHapus = "http://192.168.1.239/Api-Auth/public/api/auth/hapus-merek/"+MerekId;

        StringRequest hapus = new StringRequest(Request.Method.POST, URLHapus, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String Message = object.getString("msg");
                    Toast.makeText(EditHapusMerek.this, Message, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(EditHapusMerek.this, MerkActivity.class); startActivity(i); finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EditHapusMerek.this, "Hapus Data Gagal", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EditHapusMerek.this, "Plase Chek Your Connection", Toast.LENGTH_SHORT).show();

            }
        }){

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + mToken);
                return headers;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> formEdit = new HashMap<>();
                formEdit.put("id", MerekId);
                return formEdit;
            }
        };
        requestQueue.add(hapus);
    }
}