package com.stmikbanisaleh.fragment.ui.Kategori;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.R;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EdtKategoriActivity extends AppCompatActivity {

    private EditText edtId_Kategori, edtNama_Kategori;
    private Button btnSumbit, btnDelete;
    private Integer Id;
    private String Nama_Kategori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edt_kategori);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.kategori) + "</font>"));

        edtId_Kategori = findViewById(R.id.id_kategori);
        edtNama_Kategori = findViewById(R.id.nama_kategori);
        btnSumbit = findViewById(R.id.editKat);
        btnDelete = findViewById(R.id.btn_delete_kat);
        Id = getIntent().getIntExtra("id", 0);
        Nama_Kategori = getIntent().getStringExtra("nama_kategori");
        edtId_Kategori.setText(Id+"");
        edtNama_Kategori.setText(Nama_Kategori);

        btnSumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionEdit();

            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionDelete();
            }
        });
    }
    public void ActionEdit(){
        Integer id_kategori = Integer.parseInt(edtId_Kategori.getText().toString());
        String nama_kategori = edtNama_Kategori.getText().toString();

        Call<ResponseBody> edit = RetrofitClient.getInstance().getApi().EditKategori(id_kategori, nama_kategori);
        edit.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(EdtKategoriActivity.this, "Success Edit Data", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(EdtKategoriActivity.this, KategoriActivity.class);
                startActivity(i);
                finish();
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EdtKategoriActivity.this, "Gagal", Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void ActionDelete(){
        Integer id_kategori = Integer.parseInt(edtId_Kategori.getText().toString());

        Call<ResponseBody> delete = RetrofitClient.getInstance().getApi().HapusKategori(id_kategori);
        delete.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(EdtKategoriActivity.this, "Success Delete", Toast.LENGTH_LONG).show();
                Intent i = new Intent(EdtKategoriActivity.this, KategoriActivity.class);
                startActivity(i);
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EdtKategoriActivity.this, "Failed Delete", Toast.LENGTH_LONG).show();

            }
        });

    }
}