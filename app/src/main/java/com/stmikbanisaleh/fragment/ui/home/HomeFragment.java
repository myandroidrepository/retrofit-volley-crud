package com.stmikbanisaleh.fragment.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stmikbanisaleh.fragment.Adapter.Volley.CustomerAdapter;
import com.stmikbanisaleh.fragment.Adapter.Volley.KategoriAdapter;
import com.stmikbanisaleh.fragment.ModelVolley.kategoriModelV;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.login.Kategori.DataKategori;
import com.stmikbanisaleh.fragment.ui.LoginVolley;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.CustomerVolley;
import com.stmikbanisaleh.fragment.ui.Volley.Kategori.EdtKat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment {


//    ActionBar actionBar = ((AppCompatActivity) getActivity().getSupportActionBar());
//        actionBar.setDisplayHomeAsUpEnabled(true);
//    getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.kategori) + "</font>"));
    SessionManager sessionManager ;
    private RecyclerView recyclerView;
    private List<kategoriModelV> list;
    private KategoriAdapter adapter ;
    private String URL = "http://192.168.1.239/Api-Auth/public/api/auth/get-kategori-produk/";

    private HomeViewModel homeViewModel;
    View view;
    public  HomeFragment(){

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ActionBar actionBar =   ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity ) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.kategori) + "</font>"));


        recyclerView = view.findViewById(R.id.recyclerView);
        FragmentActivity linear = getActivity();
        dataKategori();

        return view;
    }

    private void dataKategori(){
        SessionManager sessionManager = new SessionManager(getContext());
        String mToken = sessionManager.getStringData("access_token");

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getActivity(), "Success Get Data!!!", Toast.LENGTH_SHORT).show();

                list = new ArrayList<>();

                //final int d = Log.d("TAG", "Datana ", object);
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray array = object.getJSONArray("data");
                    //System.out.println(array);

                    for(int i=0; i < array.length(); i++){
                        kategoriModelV model = new kategoriModelV();
                        JSONObject data = array.getJSONObject(i);
                        model.setNama_kategori(data.getString("nama_kategori"));
                        model.setId(data.getString("id"));
                        list.add(model);
                       // System.out.println(data);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
                adapter = new KategoriAdapter(getActivity(), list);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Failed Get Data!!!", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Bearer" + mToken);
                return headers;
            }
        };
        requestQueue.add(request);
    }
}