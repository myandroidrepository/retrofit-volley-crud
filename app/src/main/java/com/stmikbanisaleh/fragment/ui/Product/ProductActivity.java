package com.stmikbanisaleh.fragment.ui.Product;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.stmikbanisaleh.fragment.Adapter.AdaperProduct;
import com.stmikbanisaleh.fragment.login.Product.DataProduct;
import com.stmikbanisaleh.fragment.login.Product.ResponProduct;
import com.stmikbanisaleh.fragment.Network.API;
import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private API api;
    private AdaperProduct adapter;
    private List<DataProduct> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product2);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.Product) + "</font>"));

        api = RetrofitClient.getInstance().getApi();
        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager =new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AdaperProduct(list);
        recyclerView.setAdapter(adapter);
        LoadDataProduct();
    }

    private void LoadDataProduct() {
        Call<ResponProduct> callProduct = api.GetProduct();
        callProduct.enqueue(new Callback<ResponProduct>() {
            @Override
            public void onResponse(Call<ResponProduct> call, Response<ResponProduct> response) {
                List<DataProduct> list = response.body().getData();
                Log.d("Retrofit Get", " Data: " + (list));
                Log.d("Retrofit Get", " Jumlah Kategori: " + (list.size()));
                adapter = new AdaperProduct(list);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<ResponProduct> call, Throwable t) {
                Toast.makeText(ProductActivity.this,"Gagal Ambil Data", Toast.LENGTH_SHORT ).show();

            }
        });
    }
}