package com.stmikbanisaleh.fragment.ui.Volley.Merek;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.MerkActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddMerekActivity extends AppCompatActivity {
    private EditText namaMerek;
    private Button simpan;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_merek);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.merek) + "</font>"));
        sessionManager = new SessionManager(this);
        namaMerek = (EditText) findViewById(R.id.namaMerek);
        simpan = (Button) findViewById(R.id.SimpanMerek);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simpanMerek();
            }


        });

    }

    private void simpanMerek() {
        String mToken = sessionManager.getStringData("access_token");
        String URLADDMEREK="http://192.168.1.239/Api-Auth/public/api/auth/tambah-merek";
        final String Merek = namaMerek.getText().toString().trim();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest Add = new StringRequest(Request.Method.POST, URLADDMEREK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String Message = object.getString("msg");
                    Toast.makeText(AddMerekActivity.this, Message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AddMerekActivity.this, MerkActivity.class);
                    startActivity(intent); finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(AddMerekActivity.this, "Failed Add Data Merek", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AddMerekActivity.this, "Failed Add Data Merek, Please Chek Your Connection", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer" + mToken);
//                Log.d("token", "hasilnya Adalah   " + mToken);
                return headers;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> form = new HashMap<>();
                form.put("nama_merek", Merek);
                Log.d("tag", form.toString());
                return form;
            }
        };
        requestQueue.add(Add);
    }
}