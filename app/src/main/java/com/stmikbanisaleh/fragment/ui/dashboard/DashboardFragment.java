package com.stmikbanisaleh.fragment.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Kategori.KategoriActivity;
import com.stmikbanisaleh.fragment.ui.LoginVolley;
import com.stmikbanisaleh.fragment.ui.MerkActivity;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.CustomerVolley;
import com.stmikbanisaleh.fragment.ui.Volley.Product.ProductActivity;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import static com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager.TOKEN;

public class DashboardFragment extends Fragment {
    private CardView Customer, Product, Merk;
    private CardView Kategori;
    private Button btn_logout;
    SharedPreferences preferences;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
       // ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
   // getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SessionManager sessionManager = new SessionManager(getContext());
        HashMap<String, String> token = sessionManager.gettoken();
        String mToken = token.get(TOKEN);
        Log.d("token", "berhasil" + mToken);

        View root = inflater.inflate(R.layout.dashboard, container, false);

       // TextView view = root.findViewById(R.id.text_dashboard);
        Customer = (CardView) root.findViewById(R.id.Customer);
        Product =(CardView) root.findViewById(R.id.Product);
        Merk = (CardView) root.findViewById(R.id.Merk);
        Kategori = (CardView) root.findViewById(R.id.Kategori);
        btn_logout = (Button) root.findViewById(R.id.buttonLogout);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionLogout();
            }
        });

        Customer.setOnClickListener(this::onClick);
        Kategori.setOnClickListener(this::onClick);
        Product.setOnClickListener(this::onClick);
        Merk.setOnClickListener(this::onClick);
        return root;
    }
    public void onClick(@NotNull View v) {
        Intent i;
        switch (v.getId()){
            case R.id.Customer : i= new Intent(getActivity(), CustomerVolley.class);startActivity(i); break;
            case R.id.Kategori : i= new Intent(getActivity(), KategoriActivity.class);startActivity(i); break;
            case R.id.Product : i = new Intent(getActivity(), ProductActivity.class); startActivity(i); break;
            case R.id.Merk : i = new Intent(getActivity(), MerkActivity.class); startActivity(i); break;

            default:break;
        }
    }

    public void actionLogout(){
        preferences = this.getActivity().getSharedPreferences("OJT", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
       Intent intent = new Intent(getActivity(), LoginVolley.class);
       startActivity(intent);




    }
}