package com.stmikbanisaleh.fragment.ui.Volley.Customer;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.stmikbanisaleh.fragment.Adapter.AdapterCust;
import com.stmikbanisaleh.fragment.Adapter.Volley.CustomerAdapter;
import com.stmikbanisaleh.fragment.ModelVolley.customerModel;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.login.Customer.ListCust;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerVolley extends AppCompatActivity {
    private Button btn_add;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ListCust> list = new ArrayList<>() ;
    //private CustomerAdapter adapter;
    private AdapterCust adapter;
    private String URL = "http://192.168.1.239/Api-Auth/public/api/auth/get-customer";
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        SessionManager sessionManager = new SessionManager(getApplicationContext());
//        HashMap<String, String> token = sessionManager.gettoken();
//        String mToken = token.get(TOKEN);
//        Log.d("token", "berhasil" + mToken);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.customer) + "</font>"));

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager =new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AdapterCust(list);
        recyclerView.setAdapter(adapter);
        btn_add = findViewById(R.id.buttonAdd);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerVolley.this, CustomerAddVolley.class);
                startActivity(intent); finish();
            }
        });
        getData2();
//        HashMap<String, String> token = sessionManager.gettoken();
        String mToken = sessionManager.getStringData("access_token");

        Log.d("token", "berhasil" + mToken);
    }

    private void getData2() {
        SessionManager sessionManager = new SessionManager(this);
        String mToken = sessionManager.getStringData("access_token");


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //requestQueue.add(jsonArrayRequest);

            StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    list = new ArrayList<>();
                    try {
                        JSONObject object = new JSONObject(response);
                        JSONArray array = object.getJSONArray("data");
                        for(int i=0; i<array.length(); i++){
                            ListCust model = new ListCust();
                            JSONObject data = array.getJSONObject(i);
                            model.setNama_customer(data.getString("nama_customer"));
                            model.setEmail(data.getString("email"));
                            model.setNo_tlp(data.getString("no_tlp"));
                            model.setAlamat(data.getString("alamat"));
                           model.setId(data.getString("id"));
                            list.add(model);
                        }
                        adapter = new AdapterCust(list);
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json; charset=UTF-8");
                    // headers.put("Authorization", sessionManager.getStringData("access_token"));
                    headers.put("Authorization", "Bearer" + mToken);
                    Log.d("token", "hasilnya Adalah   " + mToken);
                    return headers;
                }
            };
            requestQueue.add(request);
    }

}
