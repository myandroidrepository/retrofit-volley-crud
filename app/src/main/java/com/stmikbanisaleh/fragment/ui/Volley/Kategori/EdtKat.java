package com.stmikbanisaleh.fragment.ui.Volley.Kategori;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.stmikbanisaleh.fragment.MainActivity;
import com.stmikbanisaleh.fragment.Network.NetworkVolley.SessionManager;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.CustomerAddVolley;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.CustomerVolley;
import com.stmikbanisaleh.fragment.ui.home.HomeFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EdtKat extends AppCompatActivity {
    Toolbar toolbar;
    String  NamaKat, IdKat;
    EditText txtNamaKat, txtIdKat;
    Button Edit, Delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edt_kat);
        Bundle bundle = this.getIntent().getExtras();
        NamaKat = bundle.getString("nama_kategori");
        IdKat = bundle.getString("id");
        Edit = findViewById(R.id.editKat);
        Delete = findViewById(R.id.btn_delete_kat);

        txtNamaKat = (EditText) findViewById(R.id.nameKat);
        txtIdKat = (EditText) findViewById(R.id.idKat);

        txtNamaKat.setText(NamaKat);
        txtIdKat.setText(IdKat);
        Edit.setOnClickListener(new View.OnClickListener() {
            String ID = txtIdKat.getText().toString();
            @Override
            public void onClick(View view) {
                EditKategori();
            }
        });
        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delKat();
            }
        });

    }

    public void EditKategori(){
        String NamaKategori = txtNamaKat.getText().toString().trim();
        String id = txtIdKat.getText().toString().trim();
        String URL = "http://192.168.1.239/Api-Auth/public/api/auth/edit-kategori-produk/"+id;

        SessionManager sessionManager = new SessionManager(this);
        String mToken = sessionManager.getStringData("access_token");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("msg");

                    Toast.makeText(EdtKat.this, message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(EdtKat.this, MainActivity.class);startActivity(intent); finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EdtKat.this, "Gagal Update", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EdtKat.this, "Plase chek Your Connection", Toast.LENGTH_SHORT).show();

            }
        }){
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Bearer" + mToken);
//                Log.d("token", "hasilnya Adalah   " + mToken);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> form = new HashMap<>();
                form.put("id", id);
                form.put("nama_kategori", NamaKategori);
                return form;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void delKat(){
        String id = txtIdKat.getText().toString().trim();
        String Url = "http://192.168.1.239/Api-Auth/public/api/auth/hapus-kategori-produk/"+id;
        SessionManager sessionManager = new SessionManager(this);
        String mToken = sessionManager.getStringData("access_token");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest requesDelete = new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String message = object.getString("msg");

                    Toast.makeText(EdtKat.this, message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(EdtKat.this, MainActivity.class);startActivity(intent); finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EdtKat.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EdtKat.this, "Plase Chek Your Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Bearer" + mToken);
//                Log.d("token", "hasilnya Adalah   " + mToken);
                return headers;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> form = new HashMap<>();
                form.put("id", id);
                return form;
            }
        };
        requestQueue.add(requesDelete);
    }
}