package com.stmikbanisaleh.fragment.ui.Customer;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.CustomerActivity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCustomerActivity extends AppCompatActivity {

    private EditText nama_customer, telp, email, alamat;
    private Button btnSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.addCustomer) + "</font>"));
         nama_customer =  findViewById(R.id.nama_customer);
         telp = findViewById(R.id.telp);
         email = findViewById(R.id.email);
         alamat = findViewById(R.id.alamat);
         btnSubmit = findViewById(R.id.submit);
         btnSubmit.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 actionAdd();
             }
         });
    }

    public void actionAdd(){
        String Nama = nama_customer.getText().toString().trim();
        String Email = email.getText().toString().trim();
        String Alamat = alamat.getText().toString().trim();
        String Telp = telp.getText().toString();

        //Validasi
        if(Nama.isEmpty()){
            nama_customer.setError("Nama Harus Diisi!!!");
            nama_customer.requestFocus();
        }
        if(Email.isEmpty()){
            email.setError("Email Harus Diisi!!!");
            email.requestFocus();
        }
        if(Alamat.isEmpty()){
            alamat.setError("Alamat Harus Diisi!!!");
            alamat.requestFocus();

        }
        if(Telp.isEmpty()){
            telp.setError("No Telp Harus Diisi!!!");
            telp.requestFocus();
        }
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().AddCustomer(Nama, Telp, Email, Alamat);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(AddCustomerActivity.this, "Success Add Data", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(AddCustomerActivity.this, CustomerActivity.class);
                startActivity(i);
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(AddCustomerActivity.this, "Add data Faile Plase Chek Your Connection!", Toast.LENGTH_SHORT).show();

            }
        });








    }






}