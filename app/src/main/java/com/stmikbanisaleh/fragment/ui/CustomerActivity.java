package com.stmikbanisaleh.fragment.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.stmikbanisaleh.fragment.Adapter.AdapterCust;
import com.stmikbanisaleh.fragment.login.Customer.ListCust;
import com.stmikbanisaleh.fragment.login.Customer.ResponCust;
import com.stmikbanisaleh.fragment.Network.API;
import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Customer.AddCustomerActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerActivity extends AppCompatActivity {
    SharedPreferences preferences;

    private Button btn_add;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterCust adapter;
    //public static ListCust list;
    private API api;
    private List<ListCust> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        btn_add = (Button) findViewById(R.id.buttonAdd);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.customer) + "</font>"));

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager =new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AdapterCust(list);
        recyclerView.setAdapter(adapter);
        api = RetrofitClient.getInstance().getApi();
        LoadData();

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerActivity.this, AddCustomerActivity.class);
                startActivity(intent); finish();
            }
        });
    }
    private void LoadData() {
        Call<ResponCust> getCustomer = api.getCustomer();
        getCustomer.enqueue(new Callback<ResponCust>() {
            @Override
            public void onResponse(Call<ResponCust> call, Response<ResponCust> response) {
              List<ListCust> list = response.body().getData();
              response.body().getData();
                Log.d("Retrofit Get", " Jumlah Customer: " + (list.size()));
                adapter = new AdapterCust(list);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<ResponCust> call, Throwable t) {
                Toast.makeText(CustomerActivity.this, "Get Data Failed", Toast.LENGTH_SHORT).show();

            }
        });
    }
}