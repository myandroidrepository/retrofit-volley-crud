package com.stmikbanisaleh.fragment.ui.Kategori;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.R;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddKategoriActivity extends AppCompatActivity {
    private EditText kategoriName;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kategori);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000000\">" + getString(R.string.kategori) + "</font>"));

        kategoriName = findViewById(R.id.nama_kategori);
        btnSubmit = findViewById(R.id.submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionAdd();
            }
        });



    }

    private void actionAdd() {
        String NamaKategori = kategoriName.getText().toString().trim();
        if(NamaKategori.isEmpty()){
            kategoriName.setError("Nama Harus Diisi!!!");
            kategoriName.requestFocus();
        }
        Call<ResponseBody> AddKat = RetrofitClient.getInstance().getApi().AddCategori(NamaKategori);
        AddKat.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(AddKategoriActivity.this, "Success Add Data", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(AddKategoriActivity.this, KategoriActivity.class);
                startActivity(i);
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(AddKategoriActivity.this, "Failed Add Data", Toast.LENGTH_SHORT).show();
            }
        });


    }
}