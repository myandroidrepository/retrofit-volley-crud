package com.stmikbanisaleh.fragment.Network;

import android.content.Context;
import android.content.SharedPreferences;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private  static  final String BASE_URL = "http://192.168.1.239/Api-Auth/public/api/auth/";
    private static RetrofitClient retrofitClient;
    private static Retrofit retrofit;

    public RetrofitClient(){
        if(retrofit == null){
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
    }
    public static void setUpToken(Context context){
        SharedPreferences preferences = context.getSharedPreferences("OJT", Context.MODE_PRIVATE);
        String token = preferences.getString("Access_Token", "");
        TokenInterceptor tokenInterceptor = new TokenInterceptor(token);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(tokenInterceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public static synchronized RetrofitClient getInstance(){
        if(retrofitClient == null){
            retrofitClient = new RetrofitClient();
        }
        return retrofitClient;
    }
    public API getApi(){
        return retrofit.create(API.class);
    }

}
