package com.stmikbanisaleh.fragment.Network;


import com.stmikbanisaleh.fragment.login.Customer.ResponCust;
import com.stmikbanisaleh.fragment.login.Kategori.ResponKategori;
import com.stmikbanisaleh.fragment.login.Product.ResponProduct;
import com.stmikbanisaleh.fragment.login.RequestLogin;
import com.stmikbanisaleh.fragment.login.ResponLogin;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface API {

    //Login
    @POST("login")
    Call<ResponLogin> Login(@Body RequestLogin requestLogin);

    //For Register API
    @FormUrlEncoded
    @POST("register")
    Call<ResponseBody> RegisterUser(
            @Field("name") String Name,
            @Field("email") String Email,
            @Field("password") String Password
    );
    @GET("get-customer")
    Call<ResponCust> getCustomer();


    @FormUrlEncoded
    @POST("tambah-customer")
    Call<ResponseBody> AddCustomer(
            @Field("nama_customer") String Nama_customer,
            @Field("no_tlp") String No_telp,
            @Field("email") String Email,
            @Field("alamat") String Alamat
    );

//    @POST("get-edit-customer/{id}")
//    Call<ResponseEdit> callData (@Path(value = "id", encoded = true) Integer id);

    @FormUrlEncoded
    @POST("update-customer/{id}")
    Call<ResponseBody> EditCustomer(
            @Path("id") Integer Id,
            @Field("nama_customer") String Nama_customer,
            @Field("no_tlp") String No_telp,
            @Field("email") String Email,
            @Field("alamat") String Alamat

    );
    @POST("hapus-customer/{id}")
    Call<ResponseBody> HapusCustomer(@Path("id") Integer id);

//    KATEGORI API
    @GET("get-kategori-produk")
    Call<ResponKategori> getKategori();


    @FormUrlEncoded
    @POST("add-kategori")
    Call<ResponseBody> AddCategori(@Field("nama_kategori") String Nama_kategori);

    @FormUrlEncoded
    @POST("edit-kategori-produk/{id}")
    Call<ResponseBody> EditKategori(@Path("id") Integer Id,
                                    @Field("nama_kategori") String nama_kategori);

    @POST("hapus-kategori-produk/{id}")
    Call<ResponseBody> HapusKategori(@Path("id") Integer id);

    @GET("get-produk")
    Call<ResponProduct> GetProduct();
}
