package com.stmikbanisaleh.fragment.Network.NetworkVolley;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.stmikbanisaleh.fragment.MainActivity;
import com.stmikbanisaleh.fragment.ui.LoginVolley;

import java.util.HashMap;
import java.util.Map;

public class SessionManager {
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public Context context;
    int PRIVATE_MODE = 0;

    public static  final String PREF_NAME = "FRAGMENT";
    public static final String TOKEN = "acces_token";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static  final String LOGIN = "IS_LOGIN";

    public SessionManager(Context context){
        this.context= context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }
    private String access_token;

    public String getAccess_token(){
        access_token = preferences.getString(TOKEN, "");
        return access_token;
    }

    public void setAccess_token(String access_token){
        this.access_token = access_token;
        preferences.edit().putString(TOKEN, access_token).apply();

    }


    public String createSession(String token){
        editor.putBoolean(LOGIN, true);
        editor.putString(TOKEN, token);
        editor.apply();

        return token;
    }

    public boolean is_login(){
        return preferences.getBoolean(LOGIN, false);
    }
    public void chekLogin(){
        if(!this.is_login()){
            Intent intent = new Intent(context, LoginVolley.class);
            context.startActivity(intent);
            ((MainActivity)context).finish();
        }
    }
    public HashMap<String, String> gettoken(){
        HashMap<String, String> user = new  HashMap<>();
        user.put(TOKEN, preferences.getString(TOKEN, null));
        return user;
    }

    public void Logout(){
        editor.clear();
        editor.commit();
        Intent intent = new Intent(context, LoginVolley.class);
        context.startActivity(intent);
        ((MainActivity)context).finish();
    }


    public boolean saveStringData(String key, String value) {
        SharedPreferences.Editor editor=preferences.edit ();
        editor.putString (key, value);
        return editor.commit ();
    }

    public String getStringData(String key) {
        String value=preferences.getString (key, "");
        return value;
    }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Bearer" + preferences.getString("access_token", null));
             //   Log.d("token", "hasilnya Adalah   " + mToken);
                return headers;
            }

}
