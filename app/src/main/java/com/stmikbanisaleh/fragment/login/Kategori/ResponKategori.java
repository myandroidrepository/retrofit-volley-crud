package com.stmikbanisaleh.fragment.login.Kategori;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponKategori {
    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("data")
    @Expose
    private List<DataKategori> data;

    public ResponKategori() {
    }

    public ResponKategori(String msg, List<DataKategori> data) {
        this.msg = msg;
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataKategori> getData() {
        return data;
    }

    public void setData(List<DataKategori> data) {
        this.data = data;
    }
}
