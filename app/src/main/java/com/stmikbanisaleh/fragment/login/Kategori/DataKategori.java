package com.stmikbanisaleh.fragment.login.Kategori;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataKategori {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("nama_kategori")
    @Expose
    private  String nama_kategori;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("updated_at")
    @Expose
    private String updated_at;


    public DataKategori() {
    }

    public DataKategori(Integer id, String nama_kategori, String created_at, String updated_at) {
        this.id = id;
        this.nama_kategori = nama_kategori;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
