package com.stmikbanisaleh.fragment.login;

public class TransactionRespon {
    private boolean success;

    public TransactionRespon(){}
    public TransactionRespon(boolean success){this.success = success;}
    public boolean isSuccess(){return success;}
    public void setSuccess(boolean success){this.success = success;}
}
