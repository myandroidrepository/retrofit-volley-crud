package com.stmikbanisaleh.fragment.login.Customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListCust {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nama_customer")
    @Expose
    private String nama_customer;

    @SerializedName("no_tlp")
    @Expose
    private String no_tlp;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("update_at")
    @Expose
    private String update_at;

    public ListCust() {
    }

    public ListCust(String id, String nama_customer, String no_tlp, String email, String alamat, String created_at, String update_at) {
        this.id = id;
        this.nama_customer = nama_customer;
        this.no_tlp = no_tlp;
        this.email = email;
        this.alamat = alamat;
        this.created_at = created_at;
        this.update_at = update_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_customer() {
        return nama_customer;
    }

    public void setNama_customer(String nama_customer) {
        this.nama_customer = nama_customer;
    }

    public String getNo_tlp() {
        return no_tlp;
    }

    public void setNo_tlp(String no_tlp) {
        this.no_tlp = no_tlp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }
}
