package com.stmikbanisaleh.fragment.login.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataProduct {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nama_produk")
    @Expose
    private  String nama_produk;

    @SerializedName("kategori_produk")
    @Expose
    private String id_kategori;

    @SerializedName("harga")
    @Expose
    private String harga;

    @SerializedName("merek")
    @Expose
    private String merek;

    @SerializedName("stok")
    @Expose
    private String stok;

    @SerializedName("creates_at")
    @Expose
    private String creates_at;

    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    @SerializedName("img")
    @Expose
    private String img;

    @SerializedName("nama_kategori")
    @Expose
    private  String nama_kategori;
    @SerializedName("id_merek")
    @Expose
    private String id_merek;

    public DataProduct() {
    }

    public DataProduct(String id, String nama_produk, String id_kategori, String harga, String merek, String stok, String creates_at, String updated_at, String img, String nama_kategori, String id_merek) {
        this.id = id;
        this.nama_produk = nama_produk;
        this.id_kategori = id_kategori;
        this.harga = harga;
        this.merek = merek;
        this.stok = stok;
        this.creates_at = creates_at;
        this.updated_at = updated_at;
        this.img = img;
        this.nama_kategori = nama_kategori;
        this.id_merek = id_merek;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(String id_kategori) {
        this.id_kategori = id_kategori;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getMerek() {
        return merek;
    }

    public void setMerek(String merek) {
        this.merek = merek;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getCreates_at() {
        return creates_at;
    }

    public void setCreates_at(String creates_at) {
        this.creates_at = creates_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getId_merek() {
        return id_merek;
    }

    public void setId_merek(String id_merek) {
        this.id_merek = id_merek;
    }
}
