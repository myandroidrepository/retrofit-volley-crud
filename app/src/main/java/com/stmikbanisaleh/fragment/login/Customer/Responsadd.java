package com.stmikbanisaleh.fragment.login.Customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Responsadd {
    @SerializedName("msg")
    @Expose
    private String message;

    @SerializedName("eror")
    @Expose
    private String Error;

    public Responsadd() {
    }

    public Responsadd(String message, String error) {
        this.message = message;
        Error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }
}
