package com.stmikbanisaleh.fragment.login.Customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseEdit {
    @SerializedName("msg")
    @Expose
    private  SIngleDataSuccess success;

    public SIngleDataSuccess getSuccess() {
        return success;
    }

    public void SIngleDataSuccess(SIngleDataSuccess sIngleDataSuccess){
        this.success = sIngleDataSuccess;
    }
}
