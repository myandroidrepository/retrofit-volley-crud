package com.stmikbanisaleh.fragment.login.Customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SIngleDataSuccess {
    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("data")
    @Expose


    private ListCust data;

    public ListCust getData() {
        return data;
    }

    public void setData(ListCust data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
