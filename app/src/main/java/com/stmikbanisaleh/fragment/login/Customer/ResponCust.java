package com.stmikbanisaleh.fragment.login.Customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponCust {

    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("data")
    @Expose
    private List<ListCust> data;

    public ResponCust() {
    }

    public ResponCust(String msg, List<ListCust> data) {
        this.msg = msg;
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ListCust> getData() {
        return data;
    }

    public void setData(List<ListCust> data) {
        this.data = data;
    }
}
