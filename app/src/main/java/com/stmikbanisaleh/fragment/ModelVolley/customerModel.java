package com.stmikbanisaleh.fragment.ModelVolley;

public class customerModel {

    public Integer id;
    public String nama_customer;
    public String no_tlp;
    public String email;
    public String alamat;
    public String created_at;
    public String updated_at;

    public customerModel() {
    }

    public customerModel(Integer id, String nama_customer, String no_tlp, String email, String alamat, String created_at, String updated_at) {
        this.id = id;
        this.nama_customer = nama_customer;
        this.no_tlp = no_tlp;
        this.email = email;
        this.alamat = alamat;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama_customer() {
        return nama_customer;
    }

    public void setNama_customer(String nama_customer) {
        this.nama_customer = nama_customer;
    }

    public String getNo_tlp() {
        return no_tlp;
    }

    public void setNo_tlp(String no_tlp) {
        this.no_tlp = no_tlp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
