package com.stmikbanisaleh.fragment.ModelVolley;

public class Merek {

    private String id;
    private String nama_merek;
    private String created_at;
    private  String updated_at;

    public Merek() {
    }

    public Merek(String id, String nama_merek, String created_at, String updated_at) {
        this.id = id;
        this.nama_merek = nama_merek;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_merek() {
        return nama_merek;
    }

    public void setNama_merek(String nama_merek) {
        this.nama_merek = nama_merek;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
