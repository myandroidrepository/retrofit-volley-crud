package com.stmikbanisaleh.fragment.ModelVolley;

public class kategoriModelV {

    private String id;
    private String nama_kategori;
    private String created_at;
    private String updated_at;

    public kategoriModelV() {
    }

    public kategoriModelV(String id, String nama_kategori, String created_at, String updated_at) {
        this.id = id;
        this.nama_kategori = nama_kategori;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
