package com.stmikbanisaleh.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.stmikbanisaleh.fragment.Network.RetrofitClient;
import com.stmikbanisaleh.fragment.login.RequestLogin;
import com.stmikbanisaleh.fragment.login.ResponLogin;
import com.stmikbanisaleh.fragment.Network.API;
import com.stmikbanisaleh.fragment.ui.Volley.RegisterVolley;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    private API api;
    private SharedPreferences preferences;
    private EditText email_text, password_text;
    private ProgressBar pb_login;
    private Button btn_login , register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        email_text = (EditText) findViewById(R.id.email_text);
        password_text =(EditText) findViewById(R.id.password_text);
        btn_login = (Button) findViewById(R.id.btn_login);
        register = findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, RegisterVolley.class);
                startActivity(i); finish();
            }
        });
        preferences = getSharedPreferences("OJT", Context.MODE_PRIVATE);
        api = RetrofitClient.getInstance().getApi();
        if(! preferences.getString("Access_Token", "").equals("")){
            RetrofitClient.setUpToken(this);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent); finish();
        }
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        }
        public void login() {
            String email = email_text.getText().toString().trim();
            String password = password_text.getText().toString().trim();
            //RequestLogin requestLogin = new RequestLogin(email, password);
            RequestLogin login = new RequestLogin(email, password);
            Call<ResponLogin> call= api.Login(login);
            call.enqueue(new Callback<ResponLogin>() {
                @Override
                public void onResponse(Call<ResponLogin> call, Response<ResponLogin> response) {
                    if(response.code() == 200){
                        ResponLogin responLogin = response.body();
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("Access_Token", responLogin.getAccess_token());
                        editor.apply();
                        RetrofitClient.setUpToken(Login.this);
                        Intent intent = new Intent(Login.this, MainActivity.class);
                        startActivity(intent); finish();
                    } else{
                        Toast.makeText(Login.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<ResponLogin> call, Throwable t) {
                    Toast.makeText(Login.this, "Error", Toast.LENGTH_SHORT).show();
                }
            });

        }




    }
//

