package com.stmikbanisaleh.fragment.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.stmikbanisaleh.fragment.login.Product.DataProduct;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Product.EdtProductActivity;

import java.util.ArrayList;
import java.util.List;

public class AdaperProduct extends RecyclerView.Adapter<AdaperProduct.ProductViewHolder> {
    Context context;
    private List<DataProduct> List = new ArrayList<>();
    private View.OnClickListener listener;

    public AdaperProduct(List<DataProduct> list){this.List = list;}

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product, parent, false);
        view.setOnClickListener(listener);
        AdaperProduct.ProductViewHolder holder = new AdaperProduct.ProductViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        final DataProduct list = List.get(position);
        holder.nama_produk.setText(list.getNama_produk());
        holder.merk.setText(list.getMerek());
        holder.kategori.setText(list.getNama_kategori());
        holder.harga.setText(list.getHarga());
        holder.stok.setText(list.getStok());
        if(holder.img != null){
            Picasso.get().load("http://192.168.1.239/Api-Auth/public/img-produk/" + List.get(position).getImg()).into(holder.img);
        }else {
            holder.img.setImageResource(R.drawable.imagedef);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(view.getContext(), EdtProductActivity.class);
                view.getContext().startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return List.size();
    }
    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView nama_produk, kategori, merk, harga, stok;
        ImageView img;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            nama_produk = itemView.findViewById(R.id.namaProduk);
            merk = itemView.findViewById(R.id.merkProduk);
            kategori = itemView.findViewById(R.id.kategoriProduk);
            harga = itemView.findViewById(R.id.harga);
            stok = itemView.findViewById(R.id.stok);
            img = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }
}
