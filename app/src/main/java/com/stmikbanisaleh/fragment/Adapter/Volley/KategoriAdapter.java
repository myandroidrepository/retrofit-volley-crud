package com.stmikbanisaleh.fragment.Adapter.Volley;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stmikbanisaleh.fragment.ModelVolley.customerModel;
import com.stmikbanisaleh.fragment.ModelVolley.kategoriModelV;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Volley.Kategori.EdtKat;

import java.util.ArrayList;
import java.util.List;

public class KategoriAdapter extends RecyclerView.Adapter<KategoriAdapter.KatagoriHolder> {

    private Context context;
    private List<kategoriModelV> List = new ArrayList<>();
    private View.OnClickListener listener;

    public KategoriAdapter(Context context,List<kategoriModelV> list){
        this.context = context;
        this.List = list;
    }

    @NonNull
    @Override
    public KatagoriHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_kategori, parent, false);
        view.setOnClickListener(listener);
        KategoriAdapter.KatagoriHolder holder = new KategoriAdapter.KatagoriHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull KatagoriHolder holder, int position) {
        final kategoriModelV list = List.get(position);
        //holder.id.setText(list.getId());
        holder.nama_kategori.setText(list.getNama_kategori());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EdtKat.class);
                intent.putExtra("nama_kategori", List.get(position).getNama_kategori());
                intent.putExtra("id", List.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public class KatagoriHolder extends RecyclerView.ViewHolder {
        TextView nama_kategori, id;
        public KatagoriHolder(@NonNull View itemView) {
            super(itemView);
            nama_kategori = itemView.findViewById(R.id.kategoriName);

        }
    }

}
