package com.stmikbanisaleh.fragment.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stmikbanisaleh.fragment.login.Kategori.DataKategori;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Kategori.EdtKategoriActivity;

import java.util.ArrayList;
import java.util.List;

public class AdapterKategori extends RecyclerView.Adapter<AdapterKategori.kategoriAdapterViewHolder> implements Filterable {
    SharedPreferences preferences;

    private Context context;
    private List<DataKategori> List = new ArrayList<>();
    private List<DataKategori> filteredList = new ArrayList<>();
    private View.OnClickListener listener;
    public AdapterKategori(List<DataKategori> list){
        this.List = list;
        this.filteredList = list;
    }

    @NonNull
    @Override
    public kategoriAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_kategori, parent, false);
        view.setOnClickListener(listener);
        AdapterKategori.kategoriAdapterViewHolder holder = new AdapterKategori.kategoriAdapterViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull kategoriAdapterViewHolder holder, int position) {

        final DataKategori list = List.get(position);
        holder.nama_kategori.setText(list.getNama_kategori());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(view.getContext(), EdtKategoriActivity.class);
                intent.putExtra("id", List.get(position).getId());
                intent.putExtra("nama_kategori", List.get(position).getNama_kategori());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return  filteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<DataKategori> listfilter =new ArrayList<>();
                String search = constraint.toString();
                for(DataKategori kategori : List){
                    if(kategori.getNama_kategori().toLowerCase().contains(search.toLowerCase())){
                        listfilter.add(kategori);
                    }
                }
                filteredList =listfilter;
                FilterResults results = new FilterResults();
                results.values = listfilter;
                return  results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                filteredList =(List<DataKategori>) results.values;
                notifyDataSetChanged();

            }
        };
    }

    public static class kategoriAdapterViewHolder extends RecyclerView.ViewHolder{
        TextView nama_kategori;

        public kategoriAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            nama_kategori = itemView.findViewById(R.id.kategoriName);
        }
    }


}
