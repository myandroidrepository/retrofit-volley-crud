package com.stmikbanisaleh.fragment.Adapter.Volley;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stmikbanisaleh.fragment.ModelVolley.customerModel;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.login.Customer.ListCust;
import com.stmikbanisaleh.fragment.ui.Customer.EdtCustomer;
import com.stmikbanisaleh.fragment.ui.Volley.Customer.EditCustomerVolley;

import java.util.ArrayList;
import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.CustomerHolder>{

    private Context context;
    private List<ListCust> List = new ArrayList<>();
    private View.OnClickListener listener;

    public CustomerAdapter( List<ListCust> list){
       // this.context = context;
        this.List = list;
    }

    @NonNull
    @Override
    public CustomerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_cust_voll, parent, false);
        view.setOnClickListener(listener);
        CustomerAdapter.CustomerHolder holder = new CustomerAdapter.CustomerHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerHolder holder, int position) {
        final ListCust list = List.get(position);
        holder.nama.setText(list.getNama_customer());
        holder.email.setText(list.getEmail());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), EditCustomerVolley.class);
                intent.putExtra("id", List.get(position).getId());
                intent.putExtra("nama_customer", List.get(position).getNama_customer());
                intent.putExtra("no_tlp", List.get(position).getNo_tlp());
                intent.putExtra("email", List.get(position).getEmail());
                intent.putExtra("alamat", List.get(position).getAlamat());
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public class CustomerHolder extends RecyclerView.ViewHolder{
        TextView id, nama, email;

        public CustomerHolder(@NonNull View itemView) {
            super(itemView);

            nama =itemView.findViewById(R.id.customerName);
            email = itemView.findViewById(R.id.customerEmail);
        }
    }
}
