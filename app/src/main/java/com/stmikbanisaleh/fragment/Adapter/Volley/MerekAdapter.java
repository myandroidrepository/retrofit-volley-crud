package com.stmikbanisaleh.fragment.Adapter.Volley;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stmikbanisaleh.fragment.ModelVolley.Merek;
import com.stmikbanisaleh.fragment.ModelVolley.kategoriModelV;
import com.stmikbanisaleh.fragment.R;
import com.stmikbanisaleh.fragment.ui.Volley.Kategori.EdtKat;
import com.stmikbanisaleh.fragment.ui.Volley.Merek.EditHapusMerek;

import java.util.ArrayList;
import java.util.List;

public class MerekAdapter extends RecyclerView.Adapter<MerekAdapter.MerekHolder> {

    private Context context;
    private List<Merek> List = new ArrayList<>();
    private View.OnClickListener listener;

    public MerekAdapter( List<Merek> list){
       // this.context = context;
        this.List = list;
    }


    @NonNull
    @Override
    public MerekHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_merek, parent, false);
        view.setOnClickListener(listener);
        MerekAdapter.MerekHolder holder = new MerekAdapter.MerekHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MerekHolder holder, int position) {
        final Merek list = List.get(position);
        //holder.id.setText(list.getId());
        holder.nama_merek.setText(list.getNama_merek());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), EditHapusMerek.class);
                intent.putExtra("nama_merek", List.get(position).getNama_merek());
                intent.putExtra("id", List.get(position).getId());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public class MerekHolder extends RecyclerView.ViewHolder {
        TextView nama_merek, id;
        public MerekHolder(@NonNull View itemView) {
            super(itemView);
            nama_merek = itemView.findViewById(R.id.merkName);

        }
    }
}
